import React from 'react';
import ReactDOM from 'react-dom';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import GuessingGame from './components/GuessingGame';
import guessReducer from './reducers/guess-reducer';

interface State {
    guess: string;
}

// Setup store
const store = Redux.createStore<State, Redux.AnyAction, unknown, unknown>(
    Redux.combineReducers({
        guess: guessReducer,
    }),
);

// Setup React redux
const mapStateToProps = (state: State) => {
    return {
        guess: state.guess
    }
}

const mapDispatchToProps = { 
    setGuess: (guess: string = '') => ({ type: 'SET_GUESS', guess: guess }),
}

const ConnectedGuessingGame = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(GuessingGame);

const target = document.getElementById('app');

ReactDOM.render(
    <ReactRedux.Provider store={store}>
        <ConnectedGuessingGame difficulty={100} />
    </ReactRedux.Provider>,
    target
);

// debug
(window as any).store = store;
