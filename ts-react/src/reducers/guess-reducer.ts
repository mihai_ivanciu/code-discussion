import Redux from 'redux';
import { isSetGuessAction } from '../actions/guess-actions';

export default function guessReducer(state: string = '', action: Redux.Action<any>) {
    if (isSetGuessAction(action)) {
        return action.guess;
    }

    return state;
}
