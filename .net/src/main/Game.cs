﻿using GuessingGame.HighScore;
using GuessingGame.ui;
using System;
using System.Collections.Generic;

namespace GuessingGame
{
    internal class Game
    {
        private ConsoleUi consoleUI;
        private FileHighScore highScore;
        private int number;
        private List<int> guesses;

        internal Game()
        {
            consoleUI = new ConsoleUi();
            highScore = new FileHighScore();
        }

        internal void Start()
        {
            consoleUI.ShowInstructions();

            string player = consoleUI.AskForPlayerName();
            if (player.Equals("admin"))
            {
                if (consoleUI.ShouldClearScores())
                {
                    highScore.ClearScores();
                }
                return;
            }

            while (true)
            {
                StartRound();
                highScore.AddScore(player, guesses.Count);
                consoleUI.ShowGuesses(guesses);
                consoleUI.ShowHighScores(highScore.Scores);
                bool playAgain = consoleUI.AskForAnotherGame();
                if (!playAgain)
                {
                    consoleUI.SayGoodbye();
                    break;
                }
            }
        }

        private void ThinkOfNumber()
        {
            Random rand = new Random();
            number = rand.Next(1000);
        }

        private void StartRound()
        {
            guesses = new List<int>();

            ThinkOfNumber();

            int guess = consoleUI.AskForNumberFromPlayer();
            guesses.Add(guess);
            while (guess != number)
            {
                if (guess < number)
                {
                    consoleUI.PromptHigher();
                }
                else
                {
                    consoleUI.PromptLower();
                }
                guess = consoleUI.AskForNumberFromPlayer();
                guesses.Add(guess);
            }
            consoleUI.Correct();
        }
    }
}